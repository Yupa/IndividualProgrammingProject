/** @file
   Testy jednostkowe

   @author Rafal Pragacz <rp386037@students.mimuw.edu.pl>
   @copyright Uniwersytet Warszawski
   @date 2017-05-30
*/
#include <stdio.h>
#include <stdarg.h>
#include <setjmp.h>
#include <string.h>
#include "cmocka.h"
#include "poly.h"

// FUNKCJE ATRAPY

/// Zmienna mówiąca o tym czy uruchamiać program
static jmp_buf jmp_at_exit;
/// Pomocnicza zmienna reprezentująca status zakończenia programu
static int exit_status;

/// Funkcja main modułu calc_poly.c jest zamieniana na funkcję calc_poly_main.
extern int calc_poly_main();

/// Pomocniczy bufor, do którego pisze atrapa funkcji fprintf
static char fprintf_buffer[256];
/// Pomocniczy bufor, do którego pisze atrapa funkcji printf
static char printf_buffer[256];
/// Pomocnicza zmienna reprezentująca pozycję zapisu w buforze fprintf
static int fprintf_position = 0;
/// Pomocnicza zmienna reprezentująca pozycję zapisu w buforze printf
static int printf_position = 0;

/// Atrapa funkcji fprintf sprawdzająca poprawność wypisywania na stderr.
int mock_fprintf(FILE* const file, const char *format, ...) {
  int return_value;
  va_list args;

  assert_true(file == stderr);
  assert_true(fprintf_position >= 0);
  assert_true((size_t)fprintf_position < sizeof(fprintf_buffer));

  va_start(args, format);
  return_value = vsnprintf(fprintf_buffer + fprintf_position,
                           sizeof(fprintf_buffer) - fprintf_position,
                           format,
                           args);
  va_end(args);

  fprintf_position += return_value;
  return return_value;
}

/// Atrapa funkcji printf sprawdzająca poprawność wypisywania na stdout.
int mock_printf(const char *format, ...) {
  int return_value;
  va_list args;

  assert_true(printf_position >= 0);
  assert_true((size_t)printf_position < sizeof(printf_buffer));

  va_start(args, format);
  return_value = vsnprintf(printf_buffer + printf_position,
                           sizeof(printf_buffer) - printf_position,
                           format,
                           args);
  va_end(args);

  printf_position += return_value;
  return return_value;
}

/// Pomocniczy bufor, z którego korzystają atrapy funkcji operujących na stdin.
static char input_stream_buffer[256];
/// Pomocnicza zmienna reprezentująca pozycję w buforze
static int input_stream_position = 0;
/// Pomocnicza zmienna reprezentująca ostatnią pozycję w buforze
static int input_stream_end = 0;
/// Pomocnicza zmienna reprezentująca liczbę przeczytanych znaków
int read_char_count;

/// Atrapa funkcji getchar używana do przechwycenia czytania z stdin.
int mock_getchar() {
  if (input_stream_position < input_stream_end)
    return input_stream_buffer[input_stream_position++];
  else
    return EOF;
}

/// Atrapa funkcji scanf używana do przechwycenia czytania z stdin.
int mock_scanf(const char *format, ...)   {
  va_list fmt_args;
  int ret;

  va_start(fmt_args, format);
  ret = vsscanf(input_stream_buffer + input_stream_position, format, fmt_args);
  va_end(fmt_args);

  input_stream_position += read_char_count;
  return ret;
}

/// Atrapa funkcji main
int mock_main() {
  if (!setjmp(jmp_at_exit))
    return calc_poly_main();
  return exit_status;
}

/// Atrapa funkcji exit
void mock_exit(int status) {
  exit_status = status;
  longjmp(jmp_at_exit, 1);
}


/// Funkcja inicjująca dane wejściowe dla programu korzystającego ze stdin
static void init_input_stream(const char *str) {
  input_stream_position = 0;
  strcpy(input_stream_buffer, str);
  input_stream_end = strlen(input_stream_buffer);
}

/// Funkcja wołana przed każdym testem
static int test_setup(void **state) {
  (void)state;

  printf_buffer[0] = '\0';
  fprintf_buffer[0] = '\0';
  printf_position = 0;
  fprintf_position = 0;

  return 0;
}


//TESTOWANIE PolyCompose
/** p wielomian zerowy, count równe 0 */
static void test_polyzero_1(void **state) {
  (void)state;
  Poly p_zero = PolyZero();
  Poly p_res = PolyCompose(&p_zero, 0, NULL);
  Poly p_expected = PolyZero();
  assert_true(PolyIsEq(&p_res, &p_expected));
}
/** p wielomian zerowy, count równe 1, x[0] wielomian stały */
static void test_polyzero_2(void **state) {
  (void)state;
  Poly p_zero = PolyZero();
  Poly p_coeff = PolyFromCoeff(1);
  Poly p_res = PolyCompose(&p_zero, 1, &p_coeff);
  Poly p_expected = PolyZero();
  assert_true(PolyIsEq(&p_res, &p_expected));
}
/** p wielomian stały, count równe 0 */
static void test_polycoeff_1(void **state) {
  (void)state;
  Poly p_coeff = PolyFromCoeff(1);
  Poly p_res = PolyCompose(&p_coeff, 0, NULL);
  Poly p_expected = p_coeff;
  assert_true(PolyIsEq(&p_res, &p_expected));
}
/** p wielomian stały, count równe 1, x[0] wielomian stały, różny od p */
static void test_polycoeff_2(void **state) {
  (void)state;
  Poly p_coeff = PolyFromCoeff(1);
  Poly p_coeff2 = PolyFromCoeff(2);
  Poly p_res = PolyCompose(&p_coeff, 1, &p_coeff2);
  Poly p_expected = p_coeff;
  assert_true(PolyIsEq(&p_res, &p_expected));
}
/** p wielomian x0, count równe 0 */
static void test_polyid_1(void **state) {
  (void)state;
  Poly p_coeff = PolyFromCoeff(1);
  Mono m_test = MonoFromPoly(&p_coeff, 1);
  Poly p = PolyAddMonos(1, &m_test);
  Poly p_res = PolyCompose(&p, 0, NULL);
  Poly p_expected = PolyZero();
  assert_true(PolyIsEq(&p_res, &p_expected));
  PolyDestroy(&p);
}
/** p wielomian x0, count równe 1, x[0] wielomian stały */
static void test_polyid_2(void **state) {
  (void)state;
  Poly p_coeff = PolyFromCoeff(1);
  Mono m_test = MonoFromPoly(&p_coeff, 1);
  Poly p = PolyAddMonos(1, &m_test);
  Poly p_res = PolyCompose(&p, 1, &p_coeff);
  Poly p_expected = p_coeff;
  assert_true(PolyIsEq(&p_res, &p_expected));
  PolyDestroy(&p);
}
/** p wielomian x0, count równe 1, x[0] wielomian x0 */
static void test_polyid_3(void **state) {
  (void)state;
  Poly p_coeff = PolyFromCoeff(1);
  Mono m_test = MonoFromPoly(&p_coeff, 1);
  Poly p = PolyAddMonos(1, &m_test);
  Poly p_res = PolyCompose(&p, 1, &p);
  Poly p_expected = p;
  assert_true(PolyIsEq(&p_res, &p_expected));
  PolyDestroy(&p);
  PolyDestroy(&p_res);
}
// KONIEC TESTOWANIA PolyCompose

// TESTOWANIE PARSE_COMPOSE
/** test parsowania - brak parametru */
static void test_no_arg(void **state) {
  (void) state;
  init_input_stream("COMPOSE");
  mock_main();
  assert_string_equal(printf_buffer, "");
  assert_string_equal(fprintf_buffer, "ERROR 1 WRONG COUNT\n");

}
/** test parsowania - minimalna wartość, czyli 0 */
static void test_min_val(void **state) {
  (void) state;

  init_input_stream("COMPOSE 0");
  mock_main();
  assert_string_equal(printf_buffer, "");
  assert_string_equal(fprintf_buffer, "ERROR 1 STACK UNDERFLOW\n");
}
/** test parsowania - maksymalna wartość reprezentowana w typie unsigned */
static void test_max_val(void **state) {
  (void) state;

  init_input_stream("COMPOSE 4294967295");
  mock_main();
  assert_string_equal(printf_buffer, "");
  assert_string_equal(fprintf_buffer, "ERROR 1 STACK UNDERFLOW\n");
}
/** test parsowania - wartość o jeden mniejsza od minimalnej, czyli -1 */
static void test_one_less_than_min_val(void **state) {
  (void) state;

  init_input_stream("COMPOSE -1");
  mock_main();
  assert_string_equal(printf_buffer, "");
  assert_string_equal(fprintf_buffer, "ERROR 1 WRONG COUNT\n");
}
/** test parsowania - wartość o jeden większa od maksymalnej
 * reprezentowanej w typie unsigned */
static void test_one_more_than_max_val(void **state) {
  (void) state;

  init_input_stream("COMPOSE 4294967296");
  mock_main();
  assert_string_equal(printf_buffer, "");
  assert_string_equal(fprintf_buffer, "ERROR 1 WRONG COUNT\n");
}
/** test parsowania - duża dodatnia wartość,
 * znacznie przekraczająca zakres typu unsigned */
static void test_much_more_than_max_val(void **state) {
  (void) state;

  init_input_stream("COMPOSE 777777777777777777777771231143");
  mock_main();
  assert_string_equal(printf_buffer, "");
  assert_string_equal(fprintf_buffer, "ERROR 1 WRONG COUNT\n");
}
/** test parsowania - kombinacja liter */
static void test_letter_combination(void **state) {
  (void) state;

  init_input_stream("COMPOSE SpOnGeBoB");
  mock_main();
  assert_string_equal(printf_buffer, "");
  assert_string_equal(fprintf_buffer, "ERROR 1 WRONG COUNT\n");
}
/** test parsowania - kombinacja cyfr i liter, rozpoczynająca się cyfrą */
static void test_number_and_letter_combination(void **state) {
  (void) state;

  init_input_stream("COMPOSE 1S2p3O4n5G6e7B8o9B");
  mock_main();
  assert_string_equal(printf_buffer, "");
  assert_string_equal(fprintf_buffer, "ERROR 1 WRONG COUNT\n");
}
// KONIEC TESTOWANIA PARSE_COMPOSE

/// Główna funkcja uruchamiająca testy
int main() {
  const struct CMUnitTest tests_poly_compose[] = {
    cmocka_unit_test(test_polyzero_1),
    cmocka_unit_test(test_polyzero_2),
    cmocka_unit_test(test_polycoeff_1),
    cmocka_unit_test(test_polycoeff_2),
    cmocka_unit_test(test_polyid_1),
    cmocka_unit_test(test_polyid_2),
    cmocka_unit_test(test_polyid_3),
  };
  const struct CMUnitTest tests_parse_compose[] = {
    cmocka_unit_test_setup(test_no_arg, test_setup),
    cmocka_unit_test_setup(test_min_val, test_setup),
    cmocka_unit_test_setup(test_max_val, test_setup),
    cmocka_unit_test_setup(test_one_less_than_min_val, test_setup),
    cmocka_unit_test_setup(test_one_more_than_max_val, test_setup),
    cmocka_unit_test_setup(test_much_more_than_max_val, test_setup),
    cmocka_unit_test_setup(test_letter_combination, test_setup),
    cmocka_unit_test_setup(test_number_and_letter_combination, test_setup),
  };
  return cmocka_run_group_tests(tests_poly_compose, NULL, NULL) +
          cmocka_run_group_tests(tests_parse_compose, test_setup, NULL);
}
