#!/bin/bash

PROG=release/calc_poly
DIR=tests

# Kolory do wypisywania wiadomosci
GREEN="\033[0;32m"
RED="\033[0;31m"
DEFAULT="\033[0m"

# Sprawdzamy czy zgadza sie ilosc parametr�w
# I je wczytujemy
if [[ $# == 2 ]]; then
    PROG=$1
    DIR=$2
fi

# Wlacza opcje, ktora przy braku zadnego pliku uniknie wejscia do petli
shopt -s nullglob
for inputFile in $DIR/*.in; do
  valgrind --leak-check=full --error-exitcode=15 --show-leak-kinds=all --errors-for-leak-kinds=all --track-origins=yes --log-file="filename" ./$PROG 2>/dev/null >/dev/null <$inputFile
  cat filename >> valgrind_output.txt
  printf "\n" >> valgrind_output.txt
done
exit 1
