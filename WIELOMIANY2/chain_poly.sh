#!/bin/bash

PROG=
DIR=
OK=1

# Kolory do wypisywania wiadomosci
GREEN="\033[0;32m"
RED="\033[0;31m"
DEFAULT="\033[0m"

# Sprawdzamy czy zgadza sie ilosc parametrów i je wczytujemy
if [[ $# != 2 ]]; then
    echo -e "${RED}WRONG NUMBER OF ARGUMENTS - $#${DEFAULT}"
    OK=0
else
    PROG=$1
    DIR=$2
    # Sprawdzamy czy plik i katalog istnieją
    if [[ ! -f "$PROG" ]]; then
        echo -e "${RED}FILE DOES NOT EXIST${DEFAULT}"
        OK=0
    fi
    if [[ ! -d "$DIR" ]]; then
        echo -e "${RED}DIRECTORY DOES NOT EXIST${DEFAULT}"
        OK=0
    fi
fi

# Jeśli wystąpił błąd to zwracamy 1
if [[ $OK == 0 ]]; then
  exit 1
else
    # Szukamy pliku zaczynającego się na "START"
	inputFile=
	for file in "$DIR"/*; do
	    # Rozpatrujemy tylko pliki
	    if [[ ! -f "$file" ]]; then
	        continue
	    fi
	    # Porównujemy tylko pierwszą linię z pliku
        if [[ $(head -1 "$file") == "START" ]]; then
            inputFile="$file"
            break
        fi
	done
	# Obcinamy z pliku pierwsza linie i ostatnia i wrzucamy do temp.in
	sed '1d;$d' "$inputFile" >temp.in
	./$PROG <temp.in >temp.out
	# Pobieramy ostatnia linie z pliku i patrzymy czy to STOP
	nextFile=$(tail -1 "$inputFile")
	while [[ "$nextFile" != "STOP" ]]; do
	    # Jeśli nie STOP to FILE wiec obcinamy pierwsze 5 znakow
	    # Zeby dostac nazwe nastepnego pliku
		inputFile="$DIR"/$(echo "$nextFile" | cut -c 6-)
		# Nowe wejscie to stare wyjscie i doklejony nowy plik
		cat temp.out >temp.in
		sed '$d' "$inputFile" >>temp.in
		# Uruchamiamy program i powtarzamy operacje
		./$PROG <temp.in >temp.out
		nextFile=$(tail -1 "$inputFile")
	done
	#Wypisujemy ostatnie wyjscie
	cat temp.out

	# Usuwamy pliki tymczasowe
	rm -f temp.out
	rm -f temp.in
	exit 0
fi
