#!/bin/bash

PROG=release/calc_poly
DIR=tests

# Kolory do wypisywania wiadomosci
GREEN="\033[0;32m"
RED="\033[0;31m"
DEFAULT="\033[0m"

# Sprawdzamy czy zgadza sie ilosc parametr�w
# I je wczytujemy
if [[ $# == 2 ]]; then
    PROG=$1
    DIR=$2
fi

# Wlacza opcje, ktora przy braku zadnego pliku uniknie wejscia do petli
shopt -s nullglob
for inputFile in $DIR/*.in; do
  ./$PROG <$inputFile >temporary.out 2>temporary.err
  # Sprawdzamy czy program zakonczyl sie bez bledu
  if [[ $? != 0 ]]; then
    echo -e "${RED}$inputFile - NIEPOPRAWNE WEJSCIE${DEFAULT}"
    continue
  fi
  # Ustawiamy flagi do wyjscia diagnostycznego i zwyklego
  OKTEST=1
  # Porownujemy oczekiwane i rzeczywiste wyjscia
  diff ${inputFile%in}out temporary.out >/dev/null
  # Jezeli kod bledu jest r�zny od 0, to wyjscia sie roznily
  if [[ $? != 0 ]]; then
    OKTEST=0
  fi
  # Sprawdzamy wyjscie diagnostyczne
  OKTESTERR=1
  diff ${inputFile%in}out temporary.out >/dev/null
  if [[ $? != 0 ]]; then
    OKTESTERR=0
  fi

  # Wypisujemy wyniki
  if (( $OKTEST == 1 && $OKTESTERR == 1)); then
    echo -e "${GREEN}$inputFile - OK${DEFAULT}"
  else
  	if [[ $OKTEST == 0 ]]; then
   	  echo -e "${RED}$inputFile - ZLE WYJSCIE${DEFAULT}"
  	fi
    if [[ $OKTESTERR == 0 ]]; then
      echo -e "${RED}$inputFile - ZLE WYJSCIE DIAGNOSTYCZNE${DEFAULT}"
    fi
  fi
  # Usuwamy pliki tymczasowe
  rm -f temporary.out
  rm -f temporary.err
done
exit 1
