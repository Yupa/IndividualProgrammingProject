/** @file
   Implementacja kalkulatora

   @author Rafal Pragacz <rp386037@students.mimuw.edu.pl>
   @copyright Uniwersytet Warszawski
   @date 2017-05-23
*/
#include "poly.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <assert.h>

#define MAX_ZNAKOW 15

/**
 * Struktura przechowująca wielomiany na stosie
 * w postaci wielomianu oraz wskaźnika na poprzedni element stosu
 */
typedef struct List {
  Poly poly;
  struct List *prev;
} List;

/// Zmienna globalna wskazująca na wierzchołek stosu
List *stack = NULL;
/// Zmienna globalna mówiąca, którą kolumnę obecnie wczytujemy
int col = 0;
/// Zmienna globalna mówiąca, który wiersz obecnie wczytujemy
int row = 0;
/// Zmienna globalna mówiąca jaki znak ostatnio wczytaliśmy
char c;

/// Funkcja wczytująca jeden znak z wejścia
void ReadChar() {
  c = getchar();
  col++;
}

/// Funkcja wczytująca linię do końca
void ReadTillEnd() {
  while (c != '\n' && c != EOF)
    ReadChar();
}

/// Funkcja wypisująca błąd, gdy stos jest zbyt krótki
void PrintErrorStackUnderflow() {
  fprintf(stderr, "ERROR %d STACK UNDERFLOW\n", row);
}

/// Funkcja wypisująca błąd przy parsowaniu wielomianów
void PrintError() {
  fprintf(stderr, "ERROR %d %d\n", row, col);
}

/// Funkcja wypisująca błąd przy błędnej komendzie
void PrintErrorWrongCommand() {
  fprintf(stderr, "ERROR %d WRONG COMMAND\n", row);
}

/// Funkcja wypisująca błąd przy źle podanej wartości w komedzie "AT"
void PrintErrorWrongValue() {
  fprintf(stderr, "ERROR %d WRONG VALUE\n", row);
}

/// Funkcja wypisująca błąd przy źle podanej wartości w komedzie "DEG_BY"
void PrintErrorWrongVariable() {
  fprintf(stderr, "ERROR %d WRONG VARIABLE\n", row);
}

/// Funkcja wczytująca stałą
poly_coeff_t ReadCoeff(bool *err) {
  poly_coeff_t x = 0;
  int flag = 1;
  // Jeśli liczba jest ujemna to bedziemy mnozyc wszystko przez -1
  if(c == '-') {
    flag = -1;
    ReadChar();
    // Jeśli na wejściu był sam  minus
    if(c == '\n' || c == EOF) {
      PrintError();
      *err = true;
      return 0;
    }
  }
  while(c <= '9' && c >= '0') {
    int digit = c - '0';
    // Sprawdzamy czy wczytanie następnej cyfry bedzie poprawne
    if(flag == 1) {
      if((LONG_MAX - digit) / 10 < x) {
        break;
      }
    }
    else {
      if((LONG_MIN + digit) / 10 > x) {
        break;
      }
    }
    x = x * 10 + flag * digit;
    ReadChar();
  }
  if(c != ',' && c != '\n' && c != EOF) {
    PrintError();
    *err = true;
    ReadTillEnd();
    return 0;
  }
  return x;
}

/// Funkcja wczytująca wykładnik
poly_exp_t ReadExp(bool *err) {
  poly_exp_t e = 0;
  while(c <= '9' && c >= '0') {
    int digit = c - '0';
    // Sprawdzamy czy wczytanie następnej cyfry bedzie poprawne
    if((INT_MAX - digit) / 10 < e) {;
      break;
    }
    e = e * 10 + digit;
    ReadChar();
  }
  if(c != ')') {
    PrintError();
    *err = true;
    ReadTillEnd();
    return 0;
  }
  return e;
}

/// Funkcja wczytująca wielomian
Poly ReadPoly(bool *err);

/// Funkcja wczytująca jednomian
Mono ReadMono(bool *err) {
  ReadChar();
  Poly p = ReadPoly(err);
  poly_exp_t e = 0;
  if(!*err) {
    if(c != ',') {
      PrintError();
      *err = true;
      ReadTillEnd();
    }
    else {
      ReadChar();
      e = ReadExp(err);
    }
  }
  return MonoFromPoly(&p, e);
}

Poly ReadPoly(bool *err) {
  // Jeśli mamy wczytać stałą
  if(c == '-' || (c <= '9' && c >= '0')) {
    poly_coeff_t new_coeff = ReadCoeff(err);
    if(*err)
      return PolyZero();
    return PolyFromCoeff(new_coeff);
  }
  // Jeśli mamy wczytać sumę jednomianów
  else {
    unsigned m_size = 1;
    unsigned curr_ind = 0;
    Mono *monos = (Mono*) malloc(sizeof(Mono));
    assert(monos != NULL);
    while(c == '(') {
      // Powiększamy tablice jeśli trzeba
      if(curr_ind == m_size) {
        m_size *= 2;
        Mono *new_monos = (Mono*) malloc(sizeof(Mono) * m_size);
        assert(new_monos != NULL);
        for (unsigned i = 0; i < m_size / 2; i++) {
          new_monos[i] = monos[i];
        }
        free(monos);
        monos = new_monos;
      }
      monos[curr_ind++] = ReadMono(err);
      // Sprawdzamy czy dobrze wczytaliśmy jednomian
      if (*err) {
        break;
      }

      ReadChar();
      // Jeśli koniec wczytywania to sumujemy
      if(c == ',' || c == '\n' || c == EOF) {
        Poly res = PolyAddMonos(curr_ind, monos);
        free(monos);
        return res;
      }
      // Jeśli jeszcze wczytujemy
      else if(c == '+') {
        ReadChar();
      }
      // Jeśli nie zachodzi żadne z powyższych to mamy błąd
      else {
        break;
      }
    }
    // Jeśli jeszcze nic nie zwróciliśmy to znaczy, że błąd
    for(unsigned i = 0; i < curr_ind; i++)
      MonoDestroy(&monos[i]);
    free(monos);
    *err = true;
    return PolyZero();
  }
}

/// Funkcja wypisująca wielomian
void PrintPoly(Poly *to_print);

/// Funkcja wypisująca jednomian
void PrintMono(Mono *to_print) {
  printf("(");
  PrintPoly(&to_print->p);
  printf(",%d)", to_print->exp);
}

void PrintPoly(Poly *to_print) {
  if(PolyIsCoeff(to_print)) {
    printf("%ld", to_print->cons);
    return;
  }
  PrintMono(&to_print->m[0]);
  for(int i = 1; i < to_print->m_size; i++) {
    printf("+");
    PrintMono(&to_print->m[i]);
  }
}

/// Funkcja dodająca na stos wielomian
void StackAdd(Poly *new_poly) {
  List *new_list = (List*) malloc(sizeof(List));
  assert(new_list != NULL);
  new_list->poly = *new_poly;
  new_list->prev = stack;
  stack = new_list;
}

/// Funkcja usuwająca ze stosu wielomian
void StackPop() {
  PolyDestroy(&stack->poly);
  List *to_delete = stack;
  stack = stack->prev;
  free(to_delete);
}

/// Funkcja czyszcząca zaalokowaną pamięć
void ClearMemory() {
  while(stack != NULL) {
    StackPop();
  }
}

/// Funkcja dodająca na wierzch stosu wielomian zerowy
void CalcZero() {
  Poly new_poly = PolyZero();
  StackAdd(&new_poly);
}

/// Funkcja sprawdzająca czy wielomian na wierzchu stosu jest stały
void CalcIsCoeff() {
  if (stack == NULL)
    PrintErrorStackUnderflow();
  else {
    if (PolyIsCoeff(&stack->poly))
      printf("1\n");
    else
      printf("0\n");
  }
}

/// Funkcja sprawdzająca czy wielomian na wierzchu stosu jest zerowy
void CalcIsZero() {
  if (stack == NULL)
    PrintErrorStackUnderflow();
  else {
    if (PolyIsZero(&stack->poly))
      printf("1\n");
    else
      printf("0\n");
  }
}

/// Funkcja klonująca wielomian na wierzchu stosu
void CalcClone() {
  if (stack == NULL)
    PrintErrorStackUnderflow();
  else {
    Poly new_poly = PolyClone(&stack->poly);
    StackAdd(&new_poly);
  }
}

/// Funkcja dodająca dwa wielomiany na wierzchu stosu
void CalcAdd() {
  if (stack == NULL || stack->prev == NULL)
    PrintErrorStackUnderflow();
  else {
    Poly new_poly = PolyAdd(&stack->poly, &stack->prev->poly);
    StackPop();
    StackPop();
    StackAdd(&new_poly);
  }
}

/// Funkcja mnożąca dwa wielomiany na wierzchu stosu
void CalcMul() {
  if (stack == NULL || stack->prev == NULL)
    PrintErrorStackUnderflow();
  else {
    Poly new_poly = PolyMul(&stack->poly, &stack->prev->poly);
    StackPop();
    StackPop();
    StackAdd(&new_poly);
  }
}

/// Funkcja negująca wielomian na wierzchu stosu
void CalcNeg() {
  if (stack == NULL)
    PrintErrorStackUnderflow();
  else {
    Poly new_poly = PolyNeg(&stack->poly);
    StackPop();
    StackAdd(&new_poly);
  }
}

/// Funkcja odejmująca dwa wielomiany na wierzchu stosu
void CalcSub() {
  if (stack == NULL || stack->prev == NULL)
    PrintErrorStackUnderflow();
  else {
    Poly new_poly = PolySub(&stack->poly, &stack->prev->poly);
    StackPop();
    StackPop();
    StackAdd(&new_poly);
  }
}

/// Funkcja sprawdzająca czy dwa wielomiany na wierzchu stosu są równe
void CalcIsEq() {
  if (stack == NULL || stack->prev == NULL)
    PrintErrorStackUnderflow();
  else {
    if (PolyIsEq(&stack->poly, &stack->prev->poly))
      printf("1\n");
    else
      printf("0\n");
  }
}

/// Funkcja wypisująca stopień wielomianu na wierzchu stosu
void CalcDeg() {
  if (stack == NULL)
    PrintErrorStackUnderflow();
  else
    printf("%d\n", PolyDeg(&stack->poly));
}

/** Funkcja wypisująca stopień wielomianu na wierzchu stosu,
 *  ze względu na zmienną o podanym numerze
*/
void CalcDegBy() {
  // Wczytujemy numer zmiennej
  unsigned idx = 0;
  ReadChar();
  if(c == '\n' || c == EOF) {
    PrintErrorWrongVariable();
    return;
  }
  while(c <= '9' && c >= '0') {
    int digit = c - '0';
    // Sprawdzamy czy wczytanie następnej cyfry bedzie poprawne
    if((UINT_MAX - digit) / 10 < idx) {
      break;
    }
    idx = idx * 10 + digit;
    ReadChar();
  }
  // Jeśli ostatnio wczytanym znakiem nie jest koniec pliku lub wiersza to błąd
  if(c != '\n' && c != EOF) {
    PrintErrorWrongVariable();
    ReadTillEnd();
    return;
  }
  // Jeśli nie możemy wykonać operacji to wypisujemy błąd
  if (stack == NULL)
    PrintErrorStackUnderflow();
  else
    printf("%d\n", PolyDegBy(&stack->poly, idx));
}

/** Funkcja wyliczająca wartość wielomianu ze stosu w punkcie x
 *  i wstawiająca na wierzch stosu wynik operacji
 */
void CalcAt() {
  poly_coeff_t x = 0;
  int flag = 1;
  ReadChar();
  if(c == '\n' || c == EOF) {
    PrintErrorWrongValue();
    return;
  }
  // Jeśli liczba jest ujemna to bedziemy mnozyc wszystko przez -1
  if(c == '-') {
    flag = -1;
    ReadChar();
  }
  while(c <= '9' && c >= '0') {
    int digit = c - '0';
    // Sprawdzamy czy wczytanie następnej cyfry bedzie poprawne
    if(flag == 1) {
      if((LONG_MAX - digit) / 10 < x) {
        break;
      }
    }
    else {
      if((LONG_MIN + digit) / 10 > x) {
        break;
      }
    }
    x = x * 10 + flag * digit;
    ReadChar();
  }
  // Jeśli ostatnio wczytanym znakiem nie jest koniec pliku lub wiersza to błąd
  if(c != '\n' && c != EOF) {
    PrintErrorWrongValue();
    ReadTillEnd();
    return;
  }
  if (stack == NULL)
    PrintErrorStackUnderflow();
  else {
    Poly new_poly = PolyAt(&stack->poly, x);
    StackPop();
    StackAdd(&new_poly);
  }
}

/// Funkcja wypisująca wielomian ze stosu
void CalcPrint() {
  if (stack == NULL)
    PrintErrorStackUnderflow();
  else {
    PrintPoly(&stack->poly);
    printf("\n");
  }
}

/// Funkcja usuwająca wielomian z wierzchu stosu
void CalcPop() {
  if (stack == NULL)
    PrintErrorStackUnderflow();
  else
    StackPop();
}

int main() {
  col = 0;
  row = 0;
  // Wczytujemy plik
  ReadChar();
  while (c != EOF) {
    // Wczytujemy wiersz
    row++;
    col = 1;
    // Komenda słowna
    if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
      char tab[MAX_ZNAKOW];
      for (int i = 0; i < MAX_ZNAKOW - 1; i++)
        tab[i] = 0;
      tab[0] = c;
      int i = 1;
      // Wczytujemy komendę
      while (i < 9) {
        ReadChar();
        if (c == ' ' || c == '\n' || c == EOF)
          break;
        tab[i++] = c;
      }
      if (c == ' ') {
        if (strcmp(tab, "DEG_BY") == 0)
          CalcDegBy();
        else if (strcmp(tab, "AT") == 0)
          CalcAt();
        else {
          // Jeśli wczytaliśmy złą komendę
          ReadTillEnd();
          PrintErrorWrongCommand();
        }
      }
      else if (c == '\n' || c == EOF) {
        if (strcmp(tab, "ZERO") == 0)
          CalcZero();
        else if (strcmp(tab, "IS_COEFF") == 0)
          CalcIsCoeff();
        else if (strcmp(tab, "IS_ZERO") == 0)
          CalcIsZero();
        else if (strcmp(tab, "CLONE") == 0)
          CalcClone();
        else if (strcmp(tab, "ADD") == 0)
          CalcAdd();
        else if (strcmp(tab, "MUL") == 0)
          CalcMul();
        else if (strcmp(tab, "NEG") == 0)
          CalcNeg();
        else if (strcmp(tab, "SUB") == 0)
          CalcSub();
        else if (strcmp(tab, "IS_EQ") == 0)
          CalcIsEq();
        else if (strcmp(tab, "DEG") == 0)
          CalcDeg();
        else if (strcmp(tab, "PRINT") == 0)
          CalcPrint();
        else if (strcmp(tab, "POP") == 0)
          CalcPop();
        else if (strcmp(tab, "DEG_BY") == 0)
          PrintErrorWrongVariable();
        else if (strcmp(tab, "AT") == 0)
          PrintErrorWrongValue();
        else {
          // Jeśli wczytaliśmy złą komendę
          ReadTillEnd();
          PrintErrorWrongCommand();
        }
      }
      else {
        // Jeśli wczytaliśmy złą komendę
        ReadTillEnd();
        PrintErrorWrongCommand();
      }
    }
    else {
      // Parsowanie wielomianu
      bool error = false;
      Poly new_poly = ReadPoly(&error);
      if (c != '\n' && c != EOF) {
        error = true;
        PrintError();
        ReadTillEnd();
      }
      if (!error)
        StackAdd(&new_poly);
    }
    if(c != EOF)
      ReadChar();
  }
  // Czyścimy pamięć
  ClearMemory();
  free(stack);
  return 0;
}
