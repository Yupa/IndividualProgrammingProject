#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tree.h"

#define MAX_ZNAKOW 20

int main(int argc, char **argv) {
  // Flaga mowiaca czy trzeba wypisac na wyjscie diagnostyczne
  int writeToStderr = 0;
  if (argc > 2) {
    printf("ERROR, za duzo argumentow");
    return 1;
  }
  else if (argc == 2) {
    if (strcmp(argv[1], "-v"))
    {
      printf("ERROR, zly argument: %s", argv[1]);
      return 1;
    }
    else
      writeToStderr = 1;
  }
  init();

  char tab[MAX_ZNAKOW];
  unsigned k, w;
  while (scanf("%19s", tab) == 1) {
    if (strcmp(tab, "ADD_NODE") == 0) {
      scanf("%d", &k);
      addTreeNode(k);
      printf("OK\n");
    }
    else if (strcmp(tab, "RIGHTMOST_CHILD") == 0) {
      scanf("%d", &k);
      printf("%d\n", rightmostChild(k));
    }
    else if (strcmp(tab, "DELETE_NODE") == 0) {
      scanf("%d", &k);
      deleteTreeNode(k);
      printf("OK\n");
    }
    else if (strcmp(tab, "DELETE_SUBTREE") == 0) {
      scanf("%d", &k);
      deleteSubtree(k);
      printf("OK\n");
    }
    else if (strcmp(tab, "SPLIT_NODE") == 0) {
      scanf("%d %d", &k, &w);
      splitNode(k, w);
      printf("OK\n");
    }
    if (writeToStderr) {
      fprintf(stderr, "NODES: %d\n", getCurrentNumberOfNodes());
    }
  }
  // Po zakonczeniu programu kasujemy wierzcholki z pamieci
  deleteAll();
  return 0;
}
