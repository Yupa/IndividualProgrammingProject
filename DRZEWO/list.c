#include <stdlib.h>
#include <stdio.h>

struct ListNode;

typedef struct ListNode* List;

struct ListNode {
  int data;
  List next;
  List prev;
};

// Zwraca numer danego wierzcholka
int showData(List node) {
  return node->data;
}

List getLeftBrother(List node) {
  return node->prev;
}

List getRightBrother(List node) {
  return node->next;
}

List createListNode(int data) {
  List newListNode = (List) malloc(sizeof(struct ListNode));
  newListNode->data = data;
  return newListNode;
}

// Tworzy nowy wierzcholek o numerze data i wstawia go przed wierzcholkiem node
List insertBefore(List* node, int data) {
  List newListNode = (List) malloc(sizeof(struct ListNode));
  newListNode->data = data;
  ((*node)->prev)->next = newListNode;
  newListNode->prev = (*node)->prev;
  (*node)->prev = newListNode;
  newListNode->next = (*node);
  return newListNode;
}

// Funkcja wiazaca ze soba dwie listy
void joinListNodes(List firstNode, List secondNode) {
  firstNode->next = secondNode;
  secondNode->prev = firstNode;
}

void deleteListNode(List node) {
  free(node);
}
