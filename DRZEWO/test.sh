#!/bin/bash

PAR=
PROG=
DIR=

# Kolory do wypisywania wiadomosci
GREEN="\033[0;32m"
RED="\033[0;31m"
DEFAULT="\033[0m"

# Sprawdzamy czy zgadza sie ilosc parametr�w
# I je wczytujemy
if [[ $# == 3 ]]; then
  PAR=$1
  PROG=$2
  DIR=$3
elif [[ $# == 2 ]]; then
  PROG=$1
  DIR=$2
fi

# Wlacza opcje, ktora przy braku zadnego pliku uniknie wejscia do petli
shopt -s nullglob
for inputFile in $DIR/*.in; do
  # Uruchamiamy program z parametrem lub bez w zaleznosci od potrzeb
  if [[ $# == 3 ]]; then
    ./$PROG $PAR <$inputFile >temporary.out 2>temporary.err
  else
    ./$PROG <$inputFile >temporary.out
  fi
  # Sprawdzamy czy program zakonczyl sie bez bledu
  if [[ $? != 0 ]]; then
    echo -e "${RED}$inputFile - NIEPOPRAWNE WEJSCIE${DEFAULT}"
    continue
  fi
  # Ustawiamy flagi do wyjscia diagnostycznego i zwyklego
  OKTEST=1
  OKTESTERR=-1
  # Porownujemy oczekiwane i rzeczywiste wyjscia
  diff ${inputFile%in}out temporary.out >/dev/null
  # Jezeli kod bledu jest r�zny od 0, to wyjscia sie roznily
  if [[ $? != 0 ]]; then
    OKTEST=0
  fi
  # Sprawdzamy wyjscie diagnostyczne
  if [[ $# == 3 ]]; then
    OKTESTERR=1
    diff ${inputFile%in}out temporary.out >/dev/null
    if [[ $? != 0 ]]; then
      OKTESTERR=0
    fi
  fi
  # Wypisujemy wyniki
  if [[ $OKTEST == 1 ]]; then
    echo -e "${GREEN}$inputFile - OK${DEFAULT}"
  else
    echo -e "${RED}$inputFile - ZLE WYJSCIE${DEFAULT}"
  fi
  if [[ $OKTESTERR == 1 ]]; then
    echo -e "${GREEN}$inputFile - STDERR OK${DEFAULT}"
  else 
    if [[ $OKTESTERR == 0 ]]; then
      echo -e "${RED}$inputFile - ZLE WYJSCIE DIAGNOSTYCZNE${DEFAULT}"
    fi
  fi
  # Usuwamy pliki tymczasowe
  rm -f temporary.out
  rm -f temporary.err
done
exit 1
