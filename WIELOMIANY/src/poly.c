/** @file
   Implementacja klasy wielomianów

   @author Rafal Pragacz <rp386037@students.mimuw.edu.pl>
   @copyright Uniwersytet Warszawski
   @date 2017-05-09
*/
#include <stdbool.h>
#include <assert.h>
#include <stddef.h>
#include <stdlib.h>
#include "poly.h"

void PolyDestroy(Poly *p) {
  for (int i = 0; i < p->m_size; i++)
    MonoDestroy(&(p->m[i]));
  free(p->m);
}

Poly PolyClone(const Poly *p) {
  Poly new_poly = (Poly) {.m_size = p->m_size, .cons = p->cons, .m = NULL};
  if (new_poly.m_size > 0) {
    new_poly.m = (Mono*) malloc(new_poly.m_size * sizeof(Mono));
    assert(new_poly.m != NULL);
    for (int i = 0; i < new_poly.m_size; i++)
      new_poly.m[i] = MonoClone(&(p->m[i]));
  }
  return new_poly;
}

Poly PolyAdd(const Poly *p, const Poly *q) {
  // Jeśli jeden jest zerowy to zwracamy drugi
  if (PolyIsZero(p))
    return PolyClone(q);
  else if (PolyIsZero(q))
    return PolyClone(p);
  // i,j przebiegają jednomiany z p.m, q.m, t - liczba jednomianów po zsumowaniu
  int i = 0, j = 0, t = 0;
  // Tworzymy tablicę maksymalnego rozmiaru
  Mono *new_monos = (Mono*) malloc(sizeof(Mono) * (p->m_size + q->m_size + 1));
  assert(new_monos != NULL);
  // Jeśli ktoryś z wielomianów jest stały
  if (PolyIsCoeff(p)) {
    if (PolyIsCoeff(q)) {
      free(new_monos);
      return (Poly) {.m_size = 0, .cons = p->cons + q->cons, .m = NULL};
    }
    // Jeśli drugi wielomian ma część stałą to dodajemy do niej p->cons
    if(q->m[0].exp == 0) {
      Poly new_poly = PolyAdd(p, &(q->m[0].p));
      if(PolyIsZero(&new_poly)) {
        PolyDestroy(&new_poly);
      }
      else {
        new_monos[0] = MonoFromPoly(&new_poly, 0);
        t++;
      }
      j++;
    }
    // Jeśli jej nie ma to tworzymy nowy jednomian p->cons*x^0
    else {
      Poly new_poly = PolyClone(p);
      new_monos[0] = MonoFromPoly(&new_poly, 0);
      t++;
    }
  }
  else if (PolyIsCoeff(q)) {
    free(new_monos);
    return PolyAdd(q, p);
  }
  // Wypełniamy tablicę new_monos
  while (i < p->m_size && j < q->m_size) {
    // Jeśli stopnie są różne to przepisujemy ten o mniejszym stopniu.
    if (p->m[i].exp < q->m[j].exp)
      new_monos[t++] = MonoClone(&(p->m[i++]));
    else if (p->m[i].exp > q->m[j].exp)
      new_monos[t++] = MonoClone(&(q->m[j++]));
    // Jeśli jednomiany mają równe stopnie to dodajemy ich współczynniki
    else {
      Poly new_poly = PolyAdd(&(p->m[i].p), &(q->m[j].p));
      // Jeśli suma jest różna od zera to dopisujemy do tablicy
      if (PolyIsZero(&new_poly) == false)
        new_monos[t++] = MonoFromPoly(&new_poly, p->m[i].exp);
      i++;
      j++;
    }
  }
  // Klonujemy pozostałe jednomiany
  while (i < p->m_size)
    new_monos[t++] = MonoClone(&(p->m[i++]));
  while (j < q->m_size)
    new_monos[t++] = MonoClone(&(q->m[j++]));
  
  Poly res;
  // Jeśli wielomian jest stały
  if (t == 1 && new_monos[0].exp == 0 && PolyIsCoeff(&new_monos[0].p)) {
    res = new_monos[0].p;
  }
  // Jeśli nie ma żadnych jednomianów to zwracamy wielomian zerowy
  else if (t == 0) {
    res = PolyZero();
  }
  else {
    res.m_size = t;
    res.cons = 0;
    res.m = (Mono*) malloc(t * sizeof(Mono));
    assert(res.m != NULL);
    // Przepisuje tablicę dobrej wielkości
    for (int i = 0; i < t; i++) {
      res.m[i] = new_monos[i];
    }
  }
  // Zwalnia wskaźnik
  free(new_monos);
  return res;
}

Poly PolyAddMonos(unsigned count, const Mono monos[]) {
  
  if (count == 0)
    return PolyZero();
  
  // Sortowanie pozycyjne
  Mono *sorted_monos = (Mono*) malloc(count * sizeof(Mono));
    assert(sorted_monos != NULL);
  Mono *new_monos = (Mono*) malloc(count * sizeof(Mono));
    assert(new_monos != NULL);
  poly_exp_t max_exp = 0;
  for (unsigned i = 0; i < count; i++) {
    new_monos[i] = monos[i];
    sorted_monos[i] = monos[i];
    if(monos[i].exp > max_exp)
      max_exp = monos[i].exp;
  }
  int digit = 1;
  while (max_exp / digit > 0) {
    int bucket[10] = { 0 };
    for (unsigned i = 0; i < count; i++)
      bucket[(new_monos[i].exp / digit) % 10]++;
    
    for (int i = 1; i < 10; i++)
      bucket[i] += bucket[i - 1];
    
    for (int i = count - 1; i >= 0; i--)
      sorted_monos[--bucket[(new_monos[i].exp / digit) % 10]] = new_monos[i];
      
    for (unsigned i = 0; i < count; i++)
      new_monos[i] = sorted_monos[i];
    
    digit *= 10;
  }
  
  // Sumowanie współczynników przy jednomianach o tym samym wykładniku
  int t = 0;
  for (unsigned i = 0; i < count; i++) {
    unsigned j = i + 1;
    while (j < count && sorted_monos[i].exp == sorted_monos[j].exp) {
      Poly new_poly = PolyAdd(&sorted_monos[i].p, &sorted_monos[j].p);
      MonoDestroy(&sorted_monos[i]);
      sorted_monos[i] = MonoFromPoly(&new_poly, sorted_monos[j].exp);
      MonoDestroy(&sorted_monos[j]);
      j++;
    }
    // Jeśli otrzmaliśmy zerowy współczynnik to usuwamy jednomian z pamięci
    if (PolyIsZero(&sorted_monos[i].p))
      MonoDestroy(&sorted_monos[i]);
    else
      new_monos[t++] = sorted_monos[i];
    i = j - 1;
  }
  
  // Zwracamy odpowiedni wielomian
  Poly res;
  if (t == 1 && sorted_monos[0].exp == 0 && PolyIsCoeff(&sorted_monos[0].p)) {
    res = new_monos[0].p;
  }
  else if (t == 0) {
    res = PolyZero();
  }
  else {
    res.m_size = t;
    res.cons = 0;
    res.m = (Mono*) malloc(t * sizeof(Mono));
    assert(res.m != NULL);
    // Kopiujemy tablicę dobrej wielkości
    for (int i = 0; i < t; i++)
      res.m[i] = new_monos[i];
  }
  // Zwalniamy wskaźniki
  free(sorted_monos);
  free(new_monos);
  return res;
}

/**
 * Mnoży wielomian przez wspołczynnik
 * @param[in] p : wielomian
 * @param[in] s : współczynnik
 * @return `p * s`
 */
Poly PolyMulCoeff(const Poly *p, const poly_coeff_t s) {
  // Jeśli wielomian jest stały
  if(PolyIsCoeff(p))
    return PolyFromCoeff(p->cons * s);
  Mono *new_monos= (Mono*) malloc(p->m_size * sizeof(Mono));
  assert(new_monos != NULL);
  int t = 0;
  for(int i = 0; i < p->m_size; i++) {
    Poly new_poly = PolyMulCoeff(&(p->m[i].p), s);
    // Jeśli przekręcił się licznik i dostaliśmy zerowy współczynnik
    if (PolyIsZero(&new_poly))
      PolyDestroy(&new_poly);
    else
      new_monos[t++] = MonoFromPoly(&new_poly, p->m[i].exp);
  }
  
  // Zwracamy odpowiedni wielomian
  Poly res;
  if (t == 1 && new_monos[0].exp == 0 && PolyIsCoeff(&new_monos[0].p)) {
    res = new_monos[0].p;
  }
  else if (t == 0) {
    res = PolyZero();
  }
  else {
    res.m_size = t;
    res.cons = 0;
    res.m = (Mono*) malloc (t * sizeof(Mono));
    assert(res.m != NULL);
    for (int i = 0; i < t; i++)
      res.m[i] = new_monos[i];
  }
  // Zwalniamy wskaźnik
  free(new_monos);
  return res;
}

Poly PolyMul(const Poly *p, const Poly *q) {
  if(PolyIsZero(p) || PolyIsZero(q))
    return PolyZero();
  else if (PolyIsCoeff(p))
    return PolyMulCoeff(q, p->cons);
  else if (PolyIsCoeff(q))
    return PolyMulCoeff(p, q->cons);
    
  // Tworzymy tablicę jednomianów maksymalnego rozmiaru
  Mono *monos = (Mono*) malloc(sizeof(Mono)*p->m_size*q->m_size);
  assert(monos != NULL);
  for (int i = 0; i < p->m_size; i++)
    for (int j = 0; j < q->m_size; j++) {
      // Mnożymy współczynniki kolejnych jednomianów i dopisujemy do tablicy
      Poly new_poly = PolyMul(&(p->m[i].p), &(q->m[j].p));
      int new_exp = p->m[i].exp + q->m[j].exp;
      monos[i*q->m_size + j] = MonoFromPoly(&new_poly, new_exp);
    }
  // Sumujemy powstałe jednomiany przy użyciu PolyAddMonos
  Poly res = PolyAddMonos(p->m_size * q->m_size, monos);
  free(monos);
  return res;
}

/**
 * Zwraca przeciwny jednomian
 * @param[in] m : jednomian
 * @return `-m`
 */
Mono MonoNeg(const Mono *m) {
  return (Mono) {.p = PolyNeg(&(m->p)), .exp = m->exp};
}

Poly PolyNeg(const Poly *p) {
  if (PolyIsZero(p)) {
    return PolyZero();
  }
  else if (PolyIsCoeff(p)) {
    return (Poly) {.m_size = 0, .cons = -(p->cons), .m = NULL};
  }
  else {
    Poly new_poly = (Poly) {.m_size = p->m_size, .cons = p->cons, .m = NULL};
    new_poly.m = (Mono*) malloc((new_poly.m_size) * sizeof(Mono));
    assert(new_poly.m != NULL);
    for (int i = 0; i < new_poly.m_size; i++)
      new_poly.m[i] = MonoNeg(&(p->m[i]));
    return new_poly;
  }
}

Poly PolySub(const Poly *p, const Poly *q) {
  Poly neg_q = PolyNeg(q);
  Poly res = PolyAdd(p, &neg_q);
  PolyDestroy(&neg_q);
  return res;
}

poly_exp_t PolyDegBy(const Poly *p, unsigned var_idx) {
  if (PolyIsZero(p)) {
    return -1;
  }
  else if (PolyIsCoeff(p)) {
    if (var_idx > 0)
      return -1;
    else
      return 0;
  }
  else if (var_idx == 0) {
    return p->m[p->m_size - 1].exp;
  }
  else {
    poly_exp_t maks = -1;
    for (int i = 0; i < p->m_size; i++) {
      poly_exp_t nowy = PolyDegBy(&(p->m[i].p), var_idx - 1);
      if (nowy > maks)
        maks = nowy;
    }
    return maks;
  }
}

poly_exp_t PolyDeg(const Poly *p) {
  if (PolyIsZero(p)) {
    return -1;
  }
  else if (PolyIsCoeff(p)) {
    return 0;
  }
  else {
    poly_exp_t maks = -1;
    for (int i = 0; i < p->m_size; i++) {
      poly_exp_t nowy = PolyDeg(&(p->m[i].p)) + p->m[i].exp;
      if (nowy > maks)
        maks = nowy;
    }
    return maks;
  }
}

bool PolyIsEq(const Poly *p, const Poly *q) {
  if (PolyIsCoeff(p) && PolyIsCoeff(q)) {
    return (p->cons == q->cons);
  }
  else if (p->m_size != q->m_size) {
    return false;
  }
  for (int i = 0; i < p->m_size; i++) {
    if (p->m[i].exp != q->m[i].exp || !PolyIsEq(&(p->m[i].p), &(q->m[i].p)))
      return false;
  }
  return true;
}

/**
 * Szybkie potęgowanie
 * @param[in] base: podstawa
 * @param[in] exp: wykładnik
 * @return `base^exp`
 */
poly_coeff_t QuickPower(poly_coeff_t base, poly_exp_t exp)
{
  poly_coeff_t result = 1;
  while (exp)
  {
    if (exp & 1)
      result *= base;
    exp >>= 1;
    base *= base;
  }
  return result;
}

Poly PolyAt(const Poly *p, poly_coeff_t x) {
  //PrintPoly(p, 0);
  if (PolyIsCoeff(p))
    return PolyClone(p);
  Poly res = PolyZero();
  for (int i = 0; i < p->m_size; i++)
  {
    Poly new_poly = PolyMulCoeff(&(p->m[i].p), QuickPower(x, p->m[i].exp));
    Poly tmp = PolyAdd(&res, &new_poly);
    PolyDestroy(&res);
    PolyDestroy(&new_poly);
    res = tmp;
  }
  return res;
}
