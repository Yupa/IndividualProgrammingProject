#include <stdio.h>
#include <stdlib.h>

struct NodeTree;

struct NodeTree {
  int value;
  NodeTree* left;
  NodeTree* right;
};

// je�li w drzewie BST wskazywanym przez t nie wyst�puje warto�� x, to zostaje wstawiona
void insert(NodeTree** t, int x) {
  //DEBUG
  //printf("zaczynam insert\n");
  if(*t != NULL) {
    //printf("wkladam %d - nie wlozylem %d\n", x, (*t)->value);
  }
  //printf("here\n");
  //DEBUG
  if(*t == NULL) {
    *t = (NodeTree*)malloc(sizeof(struct NodeTree));
    (*t)->value = x;
    (*t)->left = NULL;
    (*t)->right = NULL;
    //printf("wkladam %d - wlozylem %d\n", x, (*t)->value);
  }
  else if (x > (*t)->value) {
    insert( &((*t)->right), x);
  }
  else if (x < (*t)->value) {
    insert(&((*t)->left), x);
  }return;
}

// wypisuje na wyj�cie warto�ci przechowywane w drzewie BST t w porz�dku rosn�cym
void printAll(NodeTree* t) {
  //DEBUG

  //printf("DRUKUJE\n");

  //DEBUG
  if(t == NULL)
    return;
  printAll(t->left);
  printf("%d\n", t->value);
  printAll(t->right);
}

// usuwa z pami�ci drzewo t
void removeAll(NodeTree* t) {
  if(t) {
    removeAll(t->left);
    removeAll(t->right);
    free (t);
  }
  return;
}

// usuwa z niepustego drzewa BST wskazywanego przez t najmniejsz� warto�� i zwraca j� jako wynik
int removeMin(NodeTree** t) {
  if((*t)->left == NULL) {
    int x = (*t)->value;
    NodeTree* NodeTreePtr = *t;
    *t = (*t)->right;
    free (NodeTreePtr);
    return x;
  }
  else
    return removeMin(&((*t)->left));
}

// jesli w drzewie BST wskazywanym przez t znajduje sie wartosc x, usuwa
void removeValue(NodeTree** t, int x) {
  //DEBUG

  //printf("usuwam %d\n", x);;

  //DEBUG

  if (*t == NULL)
    return;
  if ((*t)->value == x) {
    if((*t)->right == NULL) {
      NodeTree* NodeTreePtr = *t;
      *t = (*t)->left;
      free (NodeTreePtr);
    }
    else
      (*t)->value = removeMin(&((*t)->right));
  }
  else if ((*t)->value > x)
    removeValue(&((*t)->right), x);
  else
    removeValue(&((*t)->left), x);
}

int main (void) {
  NodeTree* tr = NULL;
  /*
  if(tr == NULL)
    printf("test1 - OK\n");
  tr = (NodeTree*)malloc(sizeof(struct NodeTree));
  if(tr == NULL)
    printf("test2 - NIE OK\n");
  tr = NULL;
  */
  int x;
  while (scanf("%d", &x) != EOF) {
    if (x < 0)
      removeValue(&tr, -x);
    else {
      insert(&tr, x);

    }
  }
  printAll(tr);
  removeAll(tr);
  return 0;
}
