#include <stdio.h>

static void sortowanie(int *tablica, int n) {
  int i, j;

  for (i = 0; i < n; ++i) {
    for (j = 0; j < n; ++j) {
      if (tablica[i] < tablica[j]) {
        tablica[i] = tablica[i] ^ tablica[j];
        tablica[j] = tablica[j] ^ tablica[i];
        tablica[i] = tablica[i] ^ tablica[j];
      }
    }
  }
}

#define ROZMIAR_TABLICY 400

int main() {
  int tablica[ROZMIAR_TABLICY];
  int i = 0, j;

  while (scanf("%d", &tablica[i]) == 1) {
    ++i;
  }

  sortowanie(tablica, i);

  for (j = 0; j < i; ++j) {
    printf("%d\n", tablica[j]);
  }

  return 0;
}
