#!/bin/bash

test()
{
        unset a
        while read b; do
                if [ -n "$a" ]; then # Jeżeli a jest niepustym łańcuchem
                        if ((a > b)); then
                                echo "Źle: $a > $b"
                                return 1
                        fi
                fi
                a=$b
        done
        echo Dobrze
}

for f in "$@"; do # Przeglądamy wszystkie argumenty
        echo -n "$f: "
        test < "$f"
done
