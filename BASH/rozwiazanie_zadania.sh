#!/bin/bash

echo -n > temporary.in

while read wiersz; do
  slowa=($wiersz)
  # Wersja nieosługująca polskich znaków diakrytycznych
  # echo -n ${slowa[1]:0:1}${slowa[0]:0:1} | tr '[:upper:]' '[:lower:]' >> temporary.in
  # Wersja osługująca polskie znaki diakrytyczne
  echo -n ${slowa[1]:0:1}${slowa[0]:0:1} \
    | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZĄĆĘŁŃÓŚŹŻ/abcdefghijklmnopqrstuvwxyzacelnoszz/" \
    >> temporary.in
  echo ${slowa[-1]} >> temporary.in
done

sort -n -k1.3,1.8 < temporary.in
rm temporary.in
