#!/bin/bash

if [[ $# != 2 ]]; then
    # >&2 redirects standard output to standard error
    echo "Usage: $0 <amount of numbers generated> <range size>." >&2
    # exit with error code 1 to indicate an error
    exit 1
elif [[ $2 < 1 ]]; then
    echo "Range must contain at least one number." >&2
    exit 1
fi

AMOUNT=$1
RANGE=$2

for ((i = 0; i < AMOUNT; ++i)); do
    echo $((RANDOM % RANGE))
done