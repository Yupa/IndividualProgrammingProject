#!/bin/bash

while true; do
    echo -n "Write something or \"quit\" to quit: "
    read text
    if [[ "$text" == "quit" ]]; then
        exit
    else
        echo "You wrote: $text"
    fi
done